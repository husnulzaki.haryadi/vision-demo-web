function updatePage(val) {
  const Url='http://34.87.88.148:8001/get_dashboard';
  // const Url="http://localhost:5000/api/2.0/mlflow/experiments/list";
  // const Url="http://example.com/";

  $.getJSON(Url, function (result) {
    updateValue(result.result);
    adjustImageSize()
    // gender_chart.data.datasets.forEach((dataset) => {
    //   dataset.data.push(val);
    // });
    // gender_chart.update()
    console.log('updated')
  })
}

function adjustImageSize() {
  const imageContainer = $('.image-container');
  const src = $('img#feed').attr('src');
  imageContainer.css({'background-image': `url(${src})`});
  const img = $('img#feed')
  img.css('display','none')
}

function updateValue(new_value) {
  new_gender_male = new_value['gender']['Male'];
  new_gender_female = new_value['gender']['Female'];
  new_age_15 = new_value['age']['<15'];
  new_age_1525 = new_value['age']['15-25'];
  new_age_2635 = new_value['age']['26-35'];
  new_age_3645 = new_value['age']['36-45'];
  new_age_4655 = new_value['age']['46-55'];
  new_age_5665 = new_value['age']['56-65'];
  new_age_65 = new_value['age']['>65'];
  new_emotion_anger = new_value['emotion']['anger'];
  new_emotion_happy = new_value['emotion']['happy'];
  new_emotion_neutral = new_value['emotion']['neutral'];
  new_emotion_sad = new_value['emotion']['sad'];
  new_emotion_surprised = new_value['emotion']['surprised'];
  
  nv_gender = [
    new_value['gender']['Male'],
    new_value['gender']['Female'],
  ];

  nv_age = [
    new_value['age']['<15'],
    new_value['age']['15-25'],
    new_value['age']['26-35'],
    new_value['age']['36-45'],
    new_value['age']['46-55'],
    new_value['age']['56-65'],
    new_value['age']['>65'],
  ];

  nv_emotion = [
    new_value['emotion']['anger'],
    new_value['emotion']['happy'],
    new_value['emotion']['neutral'],
    new_value['emotion']['sad'],
    new_value['emotion']['surprised'],
  ];

  updateChart(gender_chart, nv_gender);
  updateChart(age_chart, nv_age);
  updateChart(emotion_chart, nv_emotion);
}

function updateChartGen(chart, new_value) {
  console.log('valgen: ' + new_value);
  chart.updateSeries(new_value)
}

function updateChart(chart, new_value) {
  console.log('val: ' + new_value);
  // chart.updateSeries(new_value)
  chart.updateSeries([{
    data: new_value,
  }]);
}

var gender_ctx = document.querySelector('#gender-chart');
var gender_chart = new ApexCharts(gender_ctx, {
  chart: {
    type: 'bar',
    height: '125',
    toolbar: {
      show: false,
    },
  },
  series: [{
    data: [20, 20],
  }],
  xaxis: {
    categories: ['Male', 'Female'],
  },
  plotOptions: {
    bar: {
      horizontal: true,
      colors: {
        // distributed: true,
        backgroundBarColors: ['#f7ce2a', '#f7ce2a'],
      },
    },
  },
  tooltip: {
    enabled: false,
  },
});
gender_chart.render();

var age_ctx = document.querySelector('#age-chart');
var age_chart = new ApexCharts(age_ctx, {
  chart: {
    type: 'bar',
    height: '125',
    toolbar: {
      show: false,
    },
  },
  series: [{
    name: 'sales',
    data: [20,20,20,20,20,20,20],
  }],
  xaxis: {
    categories: ['<15', '15-25', '26-35', '36-45', '46-55', '56-65', '>65'],
  },
  yaxis: {
    show: false,
  },
  plotOptions: {
    bar: {
      distributed: true,
      colors: {
        // backgroundBarColors: ['#249dfa', '#24e5a4', '#fcb93a', '#fd5e76', '#8973d6', '#2231f9', '#24e544'],
      //   backgroundBarOpacity: 1,
      },
    },
  },
  tooltip: {
    enabled: false,
  },
});
age_chart.render();

var emotion_ctx = document.querySelector('#emotion-chart');
var emotion_chart = new ApexCharts(emotion_ctx, {
  chart: {
    type: 'radar',
    height: '260',
    toolbar: {
      show: false,
    },
  },
  markers: {
    colors: '#f38e1b',
    size: 5,
  },
  series: [{
    data: [20,20,20,20,20],
  }],
  tooltip: {
    enabled: false,
  },
  xaxis: {
    categories: ['Anger', 'Happy', 'Neutral', 'Sad', 'Surprised'],
    labels: {
      style: {
        colors: ['#fff'],
      },
    },
  },
  yaxis: {
    show: false,
  },
  plotOptions: {
    radar: {
      polygons:{
        strokeColors: '#fcf81b',
        connectorColors: '#fcf81b',
        fill:{
          colors: ['rgba(140, 201, 34, 0.7)'],
        },
      },
    },
  },
});
emotion_chart.render();
/*
var gender_ctx = document.querySelector('#gender-chart');
var gender_chart = new ApexCharts(gender_ctx, {
  type: 'polarArea',
  data: {
    labels: ['Male', 'Female'],
    datasets: [{
      backgroundColor: [
        'rgba(247, 39, 43, 0.7)',
        'rgba(39, 39, 247, 0.7)',
      ],
      borderColor: 'rgba(63, 63, 63, 0.5)',
      borderWidth: 1,
      data: [20, 20]
    }]
  },

  options: {
    responsive: false,
    legend: {
      display: true,
      position: 'right',
    },
    scale: {
      ticks: {
        display: false,
      }
    },
    tooltips: {
      enabled: false,
    }
  }
});
*/

/*
  type: 'bar',
  data: {
    labels: ['<15', '15-25', '26-35', '36-45', '46-55', '56-65', '>65'],
    datasets: [{
      backgroundColor: [
        'rgba(242, 67, 41, 0.7)',
        'rgba(244, 167, 0, 0.7)',
        'rgba(235, 252, 5, 0.7)',
        'rgba(87, 252, 5, 0.7)',
        'rgba(5, 252, 240, 0.7)',
        'rgba(252, 5, 248, 0.7)',
        'rgba(190, 5, 252, 0.7)',
      ],
      borderColor: 'rgba(63, 63, 63, 0.5)',
      borderWidth: 1,
      data: [20, 20, 20, 20, 20, 20, 20]
    }]
  },

  options: {
    responsive: false,
    legend: {
      display: false,
    },
    scales: {
      xAxes: [{

      }],
      yAxes: [{
        ticks:{
          display: false,
        },
      }],
    },
    tooltips: {
      enabled: false,
    }
  }
});

*/

/*
{
  type: 'radar',
  data: {
    labels: ['Anger', 'Happy', 'Neutral', 'Sad', 'Surprised'],
    datasets: [{
      backgroundColor: 'rgba(240, 142, 32, 0.7)',
      pointBackgroundColor: [
        'rgba(186, 16, 16)',
        'rgba(21, 178, 7)',
        'rgba(150, 160, 146)',
        'rgba(118, 134, 211)',
        'rgba(235, 252, 5)',
      ],
      borderColor: 'rgba(63, 63, 63, 0.5)',
      borderWidth: 1,
      data: [20, 20, 20, 20, 20]
    }]
  },

  options: {
    responsive: false,
    legend: {
      display: false,
    },
    scale: {
      ticks: {
        display: false,
        suggestedMin: 0,
        suggestedMax: 50,
        stepSize: 10,
      }
    },
    tooltips: {
      enabled: false,
    }
  }
}
*/

adjustImageSize()
$(document).ready(function(){
  setInterval(updatePage, 1000);
});